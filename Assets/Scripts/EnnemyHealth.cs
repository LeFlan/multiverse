using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar; // Référence à la barre de santé de l'ennemi

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth); // Initialiser la barre de santé avec la santé maximale
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth); // Mettre à jour la barre de santé
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        // Actions à prendre lorsque l'ennemi meurt
        gameObject.SetActive(false);
    }
}

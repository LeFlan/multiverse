using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyIA : MonoBehaviour
{
    public Animator animator;
    public float speed;
    public Transform target;
    public int attackDamage = 10; 
    public float attackCooldown = 1f; // Temps entre les attaques
    public SpriteRenderer spriteRenderer;

    private int currentHealth;
    private float lastAttackTime;
    void Start () 
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    void Update () {
        /*if(Vector2.Distance(transform.position, target.position) > 0.4) 
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime); 
            Flip(transform.position.x);
        }
        else
        {
            // Vérifier le cooldown avant d'attaquer
            if (Time.time >= lastAttackTime + attackCooldown)
            {
                Attack();
                lastAttackTime = Time.time;
            }
        }*/
        float distanceToTarget = Vector2.Distance(transform.position, target.position);

        if (distanceToTarget > 0.4f)
        {
            Vector2 direction = (target.position - transform.position).normalized;
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            Flip(transform.position.x - target.position.x ); // Appeler la méthode Flip avec la direction x
        }
        else
        {
            // Vérifier le cooldown avant d'attaquer
            if (Time.time >= lastAttackTime + attackCooldown)
            {
                Attack();
                lastAttackTime = Time.time;
            }
        }
        
    }
    void Flip(float _velocity)
        {
            if (_velocity > 0f)
            {
                spriteRenderer.flipX = false;
            }
            else if (_velocity < 0f)
            {
                spriteRenderer.flipX = true;
            }
        }
    void Attack()
    {
        // Assurez-vous que le joueur a un script PlayerHealth
        PlayerHealth playerHealth = target.GetComponent<PlayerHealth>();
        if (playerHealth != null)
        {
            playerHealth.TakeDamage(attackDamage);
            animator.Play("PlayerPunch2");
        }
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage; // Réduire la santé

        if (currentHealth <= 0)
        {
            Die(); // Appeler la fonction Die lorsque la santé atteint zéro
        }
    }

    void Die()
    {
        // Mettre à jour le gameplay pour indiquer que l'ennemi est mort
        animator.SetTrigger("Die"); // Déclencher l'animation de mort
        GetComponent<Collider2D>().enabled = false; // Désactiver le collider de l'ennemi
        this.enabled = false; // Désactiver ce script

        // Activer le menu de victoire
        FindObjectOfType<WinMenu>().ShowWinMenu();
    }
}

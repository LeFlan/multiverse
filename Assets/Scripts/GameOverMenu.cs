using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public PlayerHealth playerHealth; // Référence au script de la santé du joueur
    public GameObject gameOverScreen; // Référence à l'écran de game over

    void Update()
    {
        // Vérifier si la vie du joueur est inférieure ou égale à zéro
        if (playerHealth.currentHealth <= 0)
        {
            GameOver();
        }
    }

    public  void GameOver()
    {
        // Afficher l'écran de game over
        gameOverScreen.SetActive(true);

        // Mettre le jeu en pause
        Time.timeScale = 0f;
    }
    
    public void Retry()
    {
        // Réinitialiser le temps
        Time.timeScale = 1f;

        // Recharger la scène actuelle
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        // Réinitialiser le temps
        Time.timeScale = 1f;

        // Charger la scène du menu principal (assurez-vous que la scène du menu principal est dans vos build settings)
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        // Quitter l'application
        Application.Quit();
    }
}

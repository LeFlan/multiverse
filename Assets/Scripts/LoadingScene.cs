using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    void Start()
    {
        // Démarrer la coroutine lorsque la scène est chargée
        StartCoroutine(LoadSceneAfterDelay());
    }

    private IEnumerator LoadSceneAfterDelay()
    {
        yield return new WaitForSeconds(5f); // Attend 5 secondes
        SceneManager.LoadScene("LauncherMulti"); // Charge la scène "LauncherMulti"
    }
}
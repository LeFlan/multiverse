using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject settings;
    public void STARTGAME()
    {
        SceneManager.LoadScene("Launcher");
    }
    public void Settings()
    {
        settings.SetActive(true);
    }

    public void CloseSettings()
    {
        settings.SetActive(false);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}

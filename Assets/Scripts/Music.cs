using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Music : MonoBehaviour
{
     public AudioClip soundEffect;
     private AudioSource audioSource;
    
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }
    
            Button button = GetComponent<Button>();
            if (button != null)
            {
                button.onClick.AddListener(PlaySound);
            }
        }
    
        void PlaySound()
        {
            audioSource.PlayOneShot(soundEffect);
        }
    
}

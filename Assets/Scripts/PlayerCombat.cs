using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Animator animator; 
    
    public Transform attackpoint1;
    public Transform attackpoint2;
    public Transform attackpoint;
    public float attackRange = 0.11f;
    public LayerMask enemyLayers;
    public Rigidbody2D rb;
    public int kickdamage = 10;
    public int punchdamage = 10;
    // Start is called before the first frame update
    void Start()
    {
        attackpoint = attackpoint1;
    }

    void Awake()
    {
        ActualAttack();
    }


    void ActualAttack()
    {
        if (rb.velocity.x > 0.1f)
        {
            attackpoint = attackpoint1;
        }
        else if (rb.velocity.x < -0.1f)
        {
            attackpoint = attackpoint2;
        }
    }

    // Update is called once per frame
    void Update()
    {
         ActualAttack();
         if(Input.GetKeyDown(KeyCode.Mouse0)) // left clic
        {
             Attack("PlayerPunch",punchdamage);
        }

        if(Input.GetKeyDown(KeyCode.Mouse1)) // right clic
        {
            Attack("PlayerKick",kickdamage);
        }
    }


    void Attack(string typeattack,int damage)
    {
        animator.Play(typeattack);
        
        Collider2D[] hitEnnemies = Physics2D.OverlapCircleAll(attackpoint.position, attackRange, enemyLayers); // pour savoir le nombre d'ennemi

        foreach(Collider2D enemy in hitEnnemies)
        {
            if((GetComponent<Collider2D>()) == enemy)
            {
                continue;
            }
        
           EnnemyHealth health = enemy.GetComponent<EnnemyHealth>();

           if(health is not null)
           {
                health.TakeDamage(damage);
                (enemy.GetComponent<Animator>()).Play("EnnemyHurt");
                Debug.Log("Attacked enemy: " + enemy.name + " for " + damage + " damage.");
           }
        }
    }


    private void OnDrawGizmos()
    {
        if(attackpoint is null)
        {
            Gizmos.DrawWireSphere(attackpoint1.transform.position,attackRange);
        }
        else
        {
            Gizmos.DrawWireSphere(attackpoint.transform.position,attackRange);
        }
        
    }

}

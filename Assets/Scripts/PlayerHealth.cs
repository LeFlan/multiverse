using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public Animator animator;
    public int maxHealth = 100;
    public int currentHealth;
    private GameOverMenu gameOverManager;

    public HealthBar healthBar;
    
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        gameOverManager = GameObject.FindObjectOfType<GameOverMenu>();
        if (gameOverManager == null)
        {
            Debug.LogError("GameOverMenu non trouvé dans la scène.");
        }
    }

    void Update()
    {
        //Test barre de vie
        //if (Input.GetKeyDown(KeyCode.H))
        //{
        //    TakeDamage(20);
        //}
    }
    

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        animator.Play("PlayerHurt");
        if (currentHealth <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        // Désactiver le joueur
        gameObject.SetActive(false);

        // Appeler la méthode GameOver() du GameOverManager pour afficher l'écran de Game Over
        if (gameOverManager != null)
        {
            gameOverManager.GameOver();
        }
        else
        {
            Debug.LogError("GameOverMenu non assigné.");
        }
    }
}

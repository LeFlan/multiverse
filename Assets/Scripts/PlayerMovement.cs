using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonAnimatorView))]
public class PlayerMovement : MonoBehaviourPunCallbacks, IPunObservable
{
    PhotonView view;
    public int maxHealth = 100;
    public int currentHealth;
    private GameOverMenu gameOverManager;

    public HealthBar healthBar;

    public Transform attackpoint1;
    public Transform attackpoint2;
    public Transform attackpoint;
    public float attackRange = 0.11f;
    public LayerMask enemyLayers;
    public int kickdamage = 10;
    public int punchdamage = 10;

    public float walkSpeed = 0.5f;
    public float runSpeed = 1.2f;
    public float jumpForce;

    private bool isJumping;
    private bool isGrounded;

    public Transform Groundcheck1;
    public float groundcheckRadius;
    public LayerMask collisionLayer;

    public Rigidbody2D rb;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    private float horiMvt;

    void Start()
    {
        view = GetComponent<PhotonView>();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        gameOverManager = GameObject.FindObjectOfType<GameOverMenu>();
        if (gameOverManager == null)
        {
            Debug.LogError("GameOverMenu non trouvé dans la scène.");
        }
    }

    void ActualAttack()
    {
        if (rb.velocity.x > 0.1f)
        {
            attackpoint = attackpoint1;
        }
        else if (rb.velocity.x < -0.1f)
        {
            attackpoint = attackpoint2;
        }
    }

    void Update()
    {
        if(view.IsMine)
        {
            isGrounded = Physics2D.OverlapCircle(Groundcheck1.position, groundcheckRadius, collisionLayer);

            float currentSpeed = walkSpeed;

            if (Input.GetButton("Fire3"))
            {
                currentSpeed = runSpeed;
            }

            horiMvt = Input.GetAxis("Horizontal") * currentSpeed;

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                isJumping = true;
                animator.Play("PlayerJump");
            }

            Flip(rb.velocity.x);

            float characterVelocity = Mathf.Abs(rb.velocity.x);
            animator.SetFloat("Speed", characterVelocity);

            ActualAttack();
            if(Input.GetKeyDown(KeyCode.Mouse0)) // left click
            {
                Attack("PlayerPunch", punchdamage);
            }

            if(Input.GetKeyDown(KeyCode.Mouse1)) // right click
            {
                Attack("PlayerKick", kickdamage);
            }
        }
    }

    void FixedUpdate()
    {
        if (view.IsMine)
        {
            MovePlayer(horiMvt);
        }
    }

    void MovePlayer(float _horiMvt)
    {
        Vector2 targetVelocity = new Vector2(_horiMvt, rb.velocity.y);
        rb.velocity = targetVelocity;

        if (isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            isJumping = false;
        }
    }

    void Flip(float _velocity)
    {
        if (_velocity > 0.1f)
        {
            spriteRenderer.flipX = false;
        }
        else if (_velocity < -0.1f)
        {
            spriteRenderer.flipX = true;
        }
    }

    void Attack(string typeattack, int damage)
    {
        animator.Play(typeattack);

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackpoint.position, attackRange, enemyLayers);

        foreach (Collider2D enemy in hitEnemies)
        {
            if ((GetComponent<Collider2D>()) == enemy)
            {
                continue;
            }

            EnnemyHealth health = enemy.GetComponent<EnnemyHealth>();

            if (health != null)
            {
                health.TakeDamage(damage);
                (enemy.GetComponent<Animator>()).Play("EnnemyHurt");
                Debug.Log("Attacked enemy: " + enemy.name + " for " + damage + " damage.");
            }
        }
    }

    public void TakeDamage(int damage)
    {
        if (view.IsMine)
        {
            currentHealth -= damage;
            healthBar.SetHealth(currentHealth);
            if (currentHealth <= 0)
            {
                Die();
            }
        }
    }

    void Die()
    {
        gameObject.SetActive(false);

        if (gameOverManager != null)
        {
            gameOverManager.GameOver();
        }
        else
        {
            Debug.LogError("GameOverMenu non assigné.");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(Groundcheck1.position, groundcheckRadius);
        if (attackpoint == null)
        {
            Gizmos.DrawWireSphere(attackpoint1.transform.position, attackRange);
        }
        else
        {
            Gizmos.DrawWireSphere(attackpoint.transform.position, attackRange);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
            stream.SendNext(rb.position);
            stream.SendNext(rb.velocity);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
            rb.position = (Vector2)stream.ReceiveNext();
            rb.velocity = (Vector2)stream.ReceiveNext();
        }
    }
}

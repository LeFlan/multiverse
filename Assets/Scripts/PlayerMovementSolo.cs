using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;






public class PlayerMovementSolo : MonoBehaviour
{
    public float walkSpeed = 0.5f;
    public float runSpeed = 1.2f;
    public float jumpForce;

    private bool isJumping;
    private bool isGrounded;

    public Transform Groundcheck1;
    public float groundcheckRadius;
    public LayerMask collisionLayer;

    public Rigidbody2D rb;
    public Animator animator; 
    public SpriteRenderer spriteRenderer;
    private float horiMvt;

    void Start()
    {
    

    }

    void Update()
    {
     
            isGrounded = Physics2D.OverlapCircle(Groundcheck1.position, groundcheckRadius, collisionLayer);

        // Default to walk speed
        float currentSpeed = walkSpeed;

        // Check if running
        if (Input.GetButton("Fire3"))
        {
            currentSpeed = runSpeed;
        }

        horiMvt = Input.GetAxis("Horizontal") * currentSpeed;

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            isJumping = true;
            animator.Play("PlayerJump");
        }

        Flip(rb.velocity.x);

        float characterVelocity = Mathf.Abs(rb.velocity.x);
        animator.SetFloat("Speed", characterVelocity);
    }

    void FixedUpdate()
    {
        MovePlayer(horiMvt);
    }

    void MovePlayer(float _horiMvt)
    {
        Vector2 targetVelocity = new Vector2(_horiMvt, rb.velocity.y);
        rb.velocity = targetVelocity;

        if (isJumping == true)
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            isJumping = false;
        }
    }

    void Flip(float _velocity)
    {
        if (_velocity > 0.1f)
        {
            spriteRenderer.flipX = false;
        }
        else if (_velocity < -0.1f)
        {
            spriteRenderer.flipX = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(Groundcheck1.position, groundcheckRadius);
    }
}
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoloOrMulti : MonoBehaviour
{

    public void Multi()
    {
        SceneManager.LoadScene("Loading");

    }
    public void Solo()
    {
        SceneManager.LoadScene("Fight");
    }

}

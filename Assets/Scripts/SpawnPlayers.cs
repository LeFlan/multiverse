using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnPlayers : MonoBehaviour
{
    public GameObject playerprefab;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    public void Start(){
        Vector2 randomposition = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        PhotonNetwork.Instantiate(playerprefab.name, randomposition, Quaternion.identity);
    }

}

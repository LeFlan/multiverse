using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public GameObject winMenuUI;

    public EnnemyHealth playerHealth; // Référence au script de la santé du joueur
    public GameObject gameOverScreen; // Référence à l'écran de game over

    void Update()
    {
        // Vérifier si la vie du joueur est inférieure ou égale à zéro
        if (playerHealth.currentHealth <= 0)
        {
           ShowWinMenu();
        }
    }
    void Start()
    {
        winMenuUI.SetActive(false);  // Assurez-vous que le menu de victoire est désactivé au début
    }

    public void ShowWinMenu()
    {
        // Afficher le menu de victoire
        winMenuUI.SetActive(true);

        // Mettre le jeu en pause
        Time.timeScale = 0f;
    }
    public void MainMenu()
    {
        // Réinitialiser le temps
        Time.timeScale = 1f;

        // Charger la scène du menu principal (assurez-vous que la scène du menu principal est dans vos build settings)
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        // Quitter l'application
        Application.Quit();
    }
}
